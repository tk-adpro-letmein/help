# let-me-in-api

## Application Link
**Deployment:** https://tka3-letmein.herokuapp.com <br>
**Project Repository:** https://gitlab.com/tk-adpro-letmein/let-me-in

## API Link
**Deployment:** https://tka3-letmein-help.herokuapp.com/help/api/question <br>
**Project Repository:** https://gitlab.com/tk-adpro-letmein/help

## Pipeline Status And Coverage Report

**Pipeline** for branch `master` <br>
**Coverage** consist of  `help-api`

![pipeline status](https://gitlab.com/tk-adpro-letmein/help/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/tk-adpro-letmein/help/badges/master/coverage.svg)
