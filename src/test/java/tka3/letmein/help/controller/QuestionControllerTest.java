package tka3.letmein.help.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tka3.letmein.help.model.Question;
import tka3.letmein.help.service.QuestionServiceImpl;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.util.Arrays;

@WebMvcTest(controllers = QuestionController.class)
class QuestionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QuestionServiceImpl questionService;

    private Question question;
    private Question answer;

    @BeforeEach
    public void setUp(){
        question = new Question("create event", "bagaimana cara create event?","budi");
        question.setIdQuestion(1);

        answer = new Question("create event", "cara create event adalah dengan masuk ke halaman create event dari home page", "ayu");
        answer.setIdQuestion(2);

        question.addAnsList(answer);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetQuestion() throws Exception{
        Iterable<Question> listQuestion = Arrays.asList(question);
        when(questionService.getListQuestion()).thenReturn(listQuestion);

        mvc.perform(get("/help/api/question").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("create event"));

    }

    @Test
    void testControllerAddQuestion() throws Exception{
        when(questionService.createQuestion(question)).thenReturn(question);

        mvc.perform(post("/help/api/add-question")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(question)))
                .andExpect(jsonPath("$.title").value("create event"));
    }

    @Test
    void testControllerAddAnswer() throws Exception{
        when(questionService.createQuestion(answer)).thenReturn(answer);
        when(questionService.getQuestionById(1)).thenReturn(question);

        mvc.perform(post("/help/api/add-question/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(answer)))
                .andExpect(jsonPath("$.title").value("create event"));
    }

}