package tka3.letmein.help.service;

import tka3.letmein.help.model.Question;
import tka3.letmein.help.repository.QuestionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class QuestionServiceImplTest {
    @Mock
    private QuestionRepository questionRepository;

    @InjectMocks
    private QuestionServiceImpl questionService;

    private Question question;

    @BeforeEach
    public void setUp(){
        question = new Question();
        question.setIdQuestion(1);
        question.setTitle("Membuat event");
        question.setContent("Bagaimana cara membuat event?");
    }

    @Test
    void testServiceCreateQuestion(){
        lenient().when(questionService.createQuestion(question)).thenReturn(question);
        Question resultQuestion = questionService.createQuestion(question);
        Assertions.assertEquals(question.getIdQuestion(), resultQuestion.getIdQuestion());
    }

    @Test
    void testServiceGetListQuestion(){
        Iterable<Question> listQuestion = questionRepository.findAll();
        lenient().when(questionService.getListQuestion()).thenReturn(listQuestion);
        Iterable<Question> listQuestionResult = questionService.getListQuestion();
        Assertions.assertIterableEquals(listQuestion, listQuestionResult);
    }

    @Test
    void testServiceGetQuestionById(){
        lenient().when(questionService.getQuestionById(question.getIdQuestion())).thenReturn(question);
        Question resultQuestion = questionService.getQuestionById(question.getIdQuestion());
        Assertions.assertEquals(question.getIdQuestion(), resultQuestion.getIdQuestion());
    }
}
