package tka3.letmein.help.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tka3.letmein.help.model.Question;
import tka3.letmein.help.service.QuestionService;


@CrossOrigin(origins = {"https://tka3-letmein.herokuapp.com"}, allowedHeaders = {"*"})
@Controller
@RequestMapping(path = "/help")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping(path = "/api/question", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Question>> getQuestion() {
        return ResponseEntity.ok(questionService.getListQuestion());
    }

    @PostMapping(path = "/api/add-question")
    public ResponseEntity<Question> addQuestionApi(@RequestBody ObjectNode objectNode) {
        String title = objectNode.get("title").asText();
        String content = objectNode.get("content").asText();
        String eventOwner = objectNode.get("questionOwner").asText();
        var question = new Question(title, content, eventOwner);
        questionService.createQuestion(question);
        return ResponseEntity.ok(question);
    }

    @PostMapping(path = "/api/add-question/{idQuestion}")
    public ResponseEntity<Question> addAnswerApi(@RequestBody ObjectNode objectNode, @PathVariable(value = "idQuestion") int idQuestion) {
        var questionParent = questionService.getQuestionById(idQuestion);
        String title = objectNode.get("title").asText();
        String content = objectNode.get("content").asText();
        String eventOwner = objectNode.get("questionOwner").asText();
        var question = new Question(title, content, eventOwner);
        question.setIdParent(idQuestion);
        questionParent.addAnsList(question);
        questionService.createQuestion(question);
        return ResponseEntity.ok(question);
    }

}
