package tka3.letmein.help.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "question")
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int idQuestion;

    @Column(name = "title")
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @OneToMany(targetEntity=Question.class)
    private List<Question> ansList;

    @Column(name = "questionOwner")
    private String questionOwner;

    @Column(name = "idParent")
    private int idParent;


    public Question(String title, String content, String questionOwner){
        this.title = title;
        this.content = content;
        this.questionOwner = questionOwner;
        ansList = new ArrayList<>();
    }

    public int getIdQuestion(){
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void addAnsList(Question question){
        ansList.add(question);
    }

    public void setAnsList(List<Question> ansList){
        this.ansList = ansList;
    }

    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

}
