package tka3.letmein.help.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tka3.letmein.help.model.Question;
import tka3.letmein.help.repository.QuestionRepository;

@Service
public class QuestionServiceImpl implements QuestionService{
    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public Question createQuestion(Question question) {
        questionRepository.save(question);
        return question;
    }

    @Override
    public Iterable<Question> getListQuestion() {
        return questionRepository.findAll();
    }

    @Override
    public Question getQuestionById(int idQuestion) {
        return questionRepository.findByIdQuestion(idQuestion);
    }


}
