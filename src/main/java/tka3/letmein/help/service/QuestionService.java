package tka3.letmein.help.service;

import tka3.letmein.help.model.Question;

public interface QuestionService {

    Question createQuestion(Question question);

    Iterable<Question> getListQuestion();

    Question getQuestionById(int idQuestion);

}
