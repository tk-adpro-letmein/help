package tka3.letmein.help.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tka3.letmein.help.model.Question;


@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> {
    Question findByIdQuestion(Integer idQuestion);
}
